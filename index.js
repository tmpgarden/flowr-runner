var Hose = require('flowr-hose');
var _ = require('lodash');

var hose = new Hose({
  name: 'runner',
  port: process.env.PORT || 0,
  kue: {
    prefix: 'q',
    redis: process.env.KUE
  }
});

console.log("FLOWR.RUNNER is running...");

hose.process('serial', process.env.CONCURRENCY || 1, function(job, done){

	var jobs = job.data.params.jobs;
	var input = job.data.params.input;

	var flow = {
		progress: function(progress, total){
			job.progress(progress, total);
		},

		end: function(err, result){
      console.log("ending:", result);
			if(!err) done(null, result);
		}
	};

	serialize(jobs, input, flow);

});

hose.process('parallel', process.env.CONCURRENCY || 1, function(job, done){
	parallelize(job.data.params, function(err, result){
		done(null, result);
	});
});

function parallelize(data, cb){
	var jobs = data.jobs;
	var inputs = data.input;
	var completedJobs = 0;
	var totalJobs = jobs.length;
	var output = [];

	_.forEach(jobs, function(job, i){
		var input = Array.isArray(inputs) ? inputs[i] : inputs;
		var parsedJob = parseJob(job, input);
		var jobName = [parsedJob.service,parsedJob.method].join(':');
		hose.create(jobName, {
			title: parsedJob.title,
			params: parsedJob.params
		}, function(err, result){
      if(err){
        cb(err, null);
      } else {
	 	    next(result, i);
      }
		});
	});

	function next(data, index){
		output[index] = data;
		completedJobs++;
		if(completedJobs===totalJobs){
			cb(null, output);
		}
	}
}

function serialize(jobs, initInput, flow){
	var tmpJobs = jobs;
	var totalJobs = jobs.length;
	var completedJobs = 0;

	function next(input){
		if(completedJobs<totalJobs){
			var job = tmpJobs.shift();
			process(job, input);
		} else {
			flow.end(null, input);
		}
	}

	function process(job, input){
		var parsedJob = parseJob(job, input);
		var jobName = [job.service,job.method].join(':');
		var kueJob = hose.create(jobName, {
			title: parsedJob.title,
			params: parsedJob.params
		}, function(err, result){
      console.log("JOB COMPLETE:", result);
			completedJobs++;
			flow.progress(completedJobs, totalJobs);
		 	next(result);
		});
	}

	next(initInput);
}

var inputRegExp = new RegExp(/<INPUT\[?(\d*)\]?>/);

function parseJob(job, input){
  _.each(job.params, function(param, name){
    if(typeof param === 'string'){
      var check = inputRegExp.exec(param);
      if(check){
        if(check[1]){
          var i = parseInt(check[1]);
          job.params[name] = input[i];
        } else {
          job.params[name] = input;
        }
      }
    }
  });

  return job;
}
